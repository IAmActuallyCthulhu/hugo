---
title: "My First Game Jam"
subtitle: "Creating My First Game Using Godot"
tags: ["game-dev", "godot"]
date: 2019-01-26
draft: false
---

I created my first game [Starved for Attention](https://itch.io/jam/weekly-game-jam-80/rate/360291) for the [Weekly Game Jam - Week 80](https://itch.io/jam/weekly-game-jam-80).
